# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTIBILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
from os.path import expandvars
from pathlib import Path
import re
from typing import List, Union, Optional
import importlib

import opentimelineio as otio
from opentimelineio.opentime import RationalTime, TimeRange
from opentimelineio.schema import (
    Clip,
    ExternalReference,
    Gap,
    ImageSequenceReference,
    Stack,
    Timeline,
    Track,
)

import bpy
from bpy.types import Operator, MovieSequence, SoundSequence, MetaSequence

# ImportHelper is a helper class, defines filename and
# invoke() function which calls the file selector.
from bpy_extras.io_utils import ExportHelper, ImportHelper
from bpy.props import StringProperty, EnumProperty
from bpy.types import Operator
from operator import attrgetter

from .utils import (
    get_audio_extensions,
    get_available_adapters_items,
    get_image_extensions,
    get_video_extensions,
)

from . import adapters
from .adapters.abstracts import GenericAdapter

# Global variables
extensions = {
    "audio": get_audio_extensions(),
    "image": get_image_extensions(),
    "video": get_video_extensions(),
}

# This is a global variable that dynamically changes whenever user selects a different format.
# The importlib module is used to dynamically import the right adapter module for the active format.
# The variable will be assigned to the Adapter() class of the active adapter (e.G .adapters.xml_adapter)
# If the format has no adapter yet, it will be None. Because all adapter classes have
# the same interface we can always call e.G adapter.draw_export_options() without having to do 10 if checks.

adapter: Optional[
    GenericAdapter
] = None  # Note: Will be actually the Adapter() class of the active adapter module
# which inherits from GenericAdapter. Only annotated here for auto complete.


def update_adapter(self: bpy.types.Operator, context: bpy.types.Context) -> None:
    """
    This function is a callback for when the user changes the editorial format in
    the export or import menu. The idea is that whenever the format changes the adapter,
    that is global scope, will be updated. All adapters have the same interface.
    """
    global adapter
    module_name = self.filename_ext[1:] + "_adapter"

    try:
        importlib.invalidate_caches()

        # Try to import adapter for selected file extension. Adapter follow naming convention
        module = importlib.import_module(f"vse_io.adapters.{module_name}")

    except ImportError:
        # No adapter available for that format
        adapter = None
    else:
        # Found adapter
        # Instance adapter class
        adapter = getattr(module, "Adapter")()


def convert_path(input_path: Union[Path, str], output_path: Union[Path, str]) -> Path:
    """
    Converts input path according to the export settings and remaps relative paths to
    not have leading Blender //.
    :param input_path: Path to convert
    :param output_path: Output Path of exported editorial format (to remap paths relative to target)

    :returns: Path converted according to export settings
    """
    input_path = Path(input_path)
    output_path = Path(output_path)
    addon_prefs = bpy.context.preferences.addons[__package__].preferences

    # If no path remapping should be done, make sure relative paths don't start with
    # Blender double slash //
    if addon_prefs.path_remapping == "NONE":
        if input_path.as_posix().startswith("//"):
            input_path = Path(input_path.as_posix()[2:])

    # If absolute path option is enabled
    elif addon_prefs.path_remapping == "ABSOLUTE":
        # We have to use os.path.abspath as well, otherwise we can end up with this:
        # /my/abs/../path
        input_path = Path(os.path.abspath(bpy.path.abspath(input_path.as_posix())))

    # If relative path option is enabled
    elif addon_prefs.path_remapping == "RELATIVE_TO_TARGET":
        input_path = Path(os.path.abspath(bpy.path.abspath(input_path.as_posix())))

        # Check if paths have commonpaths, if so built relative path
        if os.path.commonpath([output_path.as_posix(), input_path.as_posix()]) not in [
            "",
            "/",
        ]:
            # For some reason path.relative_to() has troubles if path is not a full subpath of output_path
            input_path = Path(
                os.path.relpath(input_path.as_posix(), output_path.parent.as_posix())
            )

        # Path is not a subpath of output path.
        # In this case leave it absolute

    return input_path


# ============== Export ================


class SEQUENCER_OT_export_editing(Operator, ExportHelper):
    """Export VSE editing to file using OpenTimelineIO"""

    bl_idname = "sequencer.export_editing"
    bl_label = "Export Editing"

    filter_glob: StringProperty(
        default=f"*.{';*.'.join(otio.adapters.suffixes_with_defined_adapters(write=True))}",
        options={"HIDDEN"},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    filename_ext: EnumProperty(  # TODO waiting for this https://developer.blender.org/T85688
        name="Timeline format",
        description="Choose timeline format",
        items=get_available_adapters_items(write=True),
        default=".otio",
        update=update_adapter,
    )

    def invoke(self, context, event):
        # Make sure that on invoke we trigger updater adapter function.
        # Otherwise last selected format might have an adapter but it's
        # not loaded.
        update_adapter(self, context)
        return super().invoke(context, event)

    def execute(self, context):
        scene = context.scene
        sequence_editor = context.scene.sequence_editor

        # Build timeline
        # ==============
        timeline = Timeline(scene.name)

        # Build sequences tree
        self.append_sequences_tree_into_stack(
            sequence_editor.sequences,
            timeline.tracks,
            track_frame_start=scene.frame_start,
        )
        # ==============

        # TODO use timeline.to_json_file() when 0.14.0 is supported everywhere

        # Customize export for specific formats.
        kwargs = {}

        # Right now the adapter only comes in to play after the whole timeline is assembled already.
        # That means if it should add specififc metadata for each clip the .add_metadata() will loop
        # again.
        if adapter:
            # Append metadata
            # Adapter will handle the different options that user can set for the export.
            # If no options enabled timeline object will not be changed
            timeline = adapter.add_export_metadata(context, timeline)

            # Get adapters write options that work as **kwargs
            # for otio.adapters.write_to_file
            kwargs.update(adapter.get_write_options())

        # Export
        otio.adapters.write_to_file(timeline, self.filepath, **kwargs)

        return {"FINISHED"}

    def draw(self, context: bpy.types.Context) -> None:
        global adapter
        addon_prefs = context.preferences.addons[__package__].preferences
        layout = self.layout

        # Draw export format
        row = layout.row()
        row.prop(self, "filename_ext")

        # Draw path options
        row = layout.row()
        row.prop(addon_prefs, "path_remapping")

        # If active format has an adapter
        if adapter:
            # Draw export options
            adapter.draw_export_options(context, layout)

    def append_sequences_tree_into_stack(
        self,
        sequences: List[Union[MetaSequence, MovieSequence, SoundSequence]],
        main_stack: Stack,
        track_frame_start=0,
    ):
        """Append sequences tree into Stack object.

        Creates a new Stack for every meta sequence.

        :param sequences: Sequences to build the Stack from
        :param main_stack: Main Stack to append sequences tree in
        :param track_frame_start: First frame of track beginning, used with nested meta for Stacks
        """
        scene = bpy.context.scene
        scene_fps = scene.render.fps / scene.render.fps_base

        created_sequences_count = 0
        for channel_index in range(1, 32):
            track_sequences = sorted(
                [s for s in sequences if s.channel == channel_index],
                key=attrgetter("frame_final_start"),
            )

            if len(track_sequences) == 0:  # Sentinel
                continue

            # Create new track TODO manage scene.tracks names
            track = Track(f"Track {channel_index}")
            track.kind = "Audio"  # Audio by default to be changed is any other type is present in the track

            main_stack.append(track)

            last_bound = scene.frame_start
            for seq in track_sequences:
                # Create gap
                frames_gap = seq.frame_final_start - last_bound
                if frames_gap > 0:
                    gap = Gap()
                    gap.source_range = TimeRange(
                        RationalTime(0, scene_fps),
                        RationalTime(frames_gap, scene_fps),
                    )

                    track.append(gap)

                # Create element
                # -----------
                if seq.type == "META":
                    clip = Stack(seq.name)
                    self.append_sequences_tree_into_stack(
                        seq.sequences, clip, track_frame_start=seq.frame_final_start
                    )

                    # Set time
                    seq_fps = scene_fps

                elif seq.type == "MOVIE":
                    clip = Clip(seq.name)
                    filepath = convert_path(seq.filepath, self.filepath)
                    clip.media_reference = ExternalReference(filepath.as_posix())

                    # Set time
                    seq_fps = seq.fps

                elif seq.type == "SOUND":
                    clip = Clip(seq.name)
                    filepath = convert_path(seq.sound.filepath, self.filepath)
                    clip.media_reference = ExternalReference(filepath.as_posix())

                    # Set time
                    seq_fps = scene_fps

                elif seq.type == "IMAGE":
                    elements = seq.elements
                    image = elements[0]
                    filepath = convert_path(
                        Path(seq.directory, image.filename), self.filepath
                    )

                    if len(elements) == 1:  # Single image

                        # Create clip
                        clip = Clip(seq.name)
                        clip.media_reference = ExternalReference(filepath.as_posix())

                        # Set time
                        seq_fps = scene_fps

                    else:  # Image sequence
                        # Determine zero padding
                        ending_digits = re.search(
                            f"\d+?(?=\{filepath.suffix})",
                            filepath.name,
                        )
                        zero_padding = len(ending_digits[0]) if ending_digits else 0

                        # Create clip
                        clip = Clip(seq.name)
                        clip.media_reference = ImageSequenceReference(
                            name_suffix=filepath.suffix,
                            frame_zero_padding=zero_padding,
                            target_url_base=filepath.parent.as_posix(),
                        )

                        # Set time
                        seq_fps = scene_fps
                else:
                    continue

                # Set Time
                # --------
                # Set available range
                if seq.type != "META":
                    clip.media_reference.available_range = TimeRange(
                        RationalTime(0, seq_fps),
                        RationalTime(seq.frame_duration, seq_fps),
                    )

                # Set source range
                clip.source_range = TimeRange(
                    RationalTime(seq.frame_offset_start, seq_fps),
                    RationalTime(seq.frame_final_duration, seq_fps),
                )
                # --------

                # Determine Track kind
                if seq.type != "SOUND":
                    track.kind = "Video"

                # Add clip to track
                track.append(clip)

                # Set last bound
                last_bound = seq.frame_final_end

                # Avoid out of RAM
                if created_sequences_count >= 85:
                    bpy.ops.sequencer.refresh_all()
                    created_sequences_count = 0
                else:
                    created_sequences_count += 1


# Only needed if you want to add into a dynamic menu
def menu_func_export(self, context):
    self.layout.operator(
        SEQUENCER_OT_export_editing.bl_idname,
        text=f"Editing (.{', .'.join(otio.adapters.suffixes_with_defined_adapters(write=True))})",
    )


# ============== Import ================
class SEQUENCER_OT_import_editing(Operator, ImportHelper):
    """Import editing from file using OpenTimelineIO"""

    bl_idname = "sequencer.import_editing"
    bl_label = "Import Editing"

    # ImportHelper mixin class uses this
    filename_ext = ".otio"

    filter_glob: StringProperty(
        default=f"*.{';*.'.join(otio.adapters.suffixes_with_defined_adapters(read=True))}",
        options={"HIDDEN"},
        maxlen=255,  # Max internal buffer length, longer would be clamped.
    )

    def execute(self, context):
        scene = context.scene
        sequence_editor = scene.sequence_editor

        # Ensure sequence editor
        if not scene.sequence_editor:
            scene.sequence_editor_create()

            # Update
            sequence_editor = scene.sequence_editor

        # Read timeline file
        timeline = otio.adapters.read_from_file(self.filepath)

        # Set scene frame start and end
        scene.frame_start = (
            0 if timeline.global_start_time is None else timeline.global_start_time
        )
        scene.frame_end = otio.opentime.to_frames(timeline.duration()) - 1
        # TODO otio.opentime.to_frames() must be replaced by timeline.duration().to_frames() when retrocompatibility
        # won't be necessary any more, let's say 1 year after Blender 3.0 and OTIO 0.14.0 release.

        # Build tracks
        for i, track in enumerate(timeline.tracks, 1):
            self.build_composable(
                sequence_editor.sequences,
                track,
                frame_offset=scene.frame_start,
                channel_index=i,
            )

            # Add track
            scene.tracks.add().name = track.name or f"Track {i}"

        return {"FINISHED"}

    def build_composable(
        self,
        sequences_stack: List[Union[MetaSequence, MovieSequence, SoundSequence]],
        composable: Union[Stack, Track],
        channel_index=1,
        frame_offset=1,
    ) -> List[Union[MetaSequence, MovieSequence, SoundSequence]]:
        """Build composable (Stack, Track).

        Creates the sequences based on composable.range_in_parent().

        :param sequences_stack: Current sequences stack (a.k.a meta) to create given composable into
        :param composable: Composable to build
        :param channel_index: Channel to build composable, defaults to 1
        :param frame_start: First frame of clip's sequence, defaults to 1
        :return: Created sequences
        """
        created_sequences = []
        created_sequences_count = 0

        for child in composable.each_child(shallow_search=True):
            # Set frame start
            child_range_in_parent = child.range_in_parent()
            frame_start = frame_offset + otio.opentime.to_frames(
                child_range_in_parent.start_time
            )

            if type(child) is Stack:  # Build Stack
                meta = sequences_stack.new_meta(
                    child.name,
                    channel_index,
                    frame_start,
                )

                # Build into VSE
                sequences = self.build_composable(
                    meta.sequences,
                    child,
                    frame_offset=frame_start,
                )

                # Apply stack attributes to meta
                self.set_data(child, meta)

                # Update sequences to return
                sequences = [meta]

            elif type(child) is Track:  # Build Track
                # Sentinel for Track into Track, remove when solved: https://github.com/PixarAnimationStudios/OpenTimelineIO/issues/1171
                if isinstance(composable, Track):
                    self.report(
                        {"WARNING"},
                        f"'Track' is nested directly into another 'Track', this may lead to unexpected behaviour. Insert a 'Stack' as parent of:\n"
                        f"{child}",
                    )
                    continue

                sequences = self.build_composable(
                    sequences_stack,
                    child,
                    channel_index=channel_index,
                    frame_offset=frame_start,
                )

                # Increment channel
                channel_index += 1
            elif type(child) is Clip:  # Build Clip
                sequences = [
                    self.build_clip(
                        sequences_stack,
                        child,
                        composable.kind,
                        channel_index=channel_index,
                        frame_start=frame_start,
                    )
                ]

                # Avoid out of RAM
                if created_sequences_count >= 85:
                    bpy.ops.sequencer.refresh_all()
                    created_sequences_count = 0
                else:
                    created_sequences_count += 1

            else:
                sequences = []

            # Keep new sequences
            created_sequences.extend([seq for seq in sequences if seq])

        return created_sequences

    def build_clip(
        self,
        sequences_stack: bpy.types.SequencesMeta,
        clip: Clip,
        kind: str,
        channel_index=1,
        frame_start=1,
    ) -> Union[MetaSequence, MovieSequence, SoundSequence]:
        """Create clip into a sequence.

        Allows to force sound loading from movie file:
        ``if track.kind == Audio`` all movie files will be loaded as sound sequences.

        For image sequences, place holder are used by default when frames are missing.

        :param sequences_stack:
        :param clip: Clip to create into VSE
        :param kind: Way to load the media: Video or Audio
        :param channel_index: Channel to create the clip's sequence to, defaults to 1
        :param frame_start: First frame of clip's sequence, defaults to 1
        :return: Created sequence
        """
        media_reference = clip.media_reference
        source_editing_file_dir = Path(self.filepath).parent

        # Image sequence
        if type(media_reference) is ImageSequenceReference:
            sequence_directory = Path(expandvars(media_reference.target_url_base))

            # Check for relative path
            if not sequence_directory.is_absolute():
                sequence_directory = source_editing_file_dir.joinpath(
                    sequence_directory
                ).resolve()

            # Get image files
            image_files = sorted(
                sequence_directory.glob(
                    f"{media_reference.name_prefix}*{media_reference.name_suffix}"
                )
            )

            # Create images sequence
            sequence = sequences_stack.new_image(
                name=clip.name,
                filepath=str(image_files[0]),
                channel=channel_index,
                frame_start=frame_start,
            )

            # Add images of sequence
            frame = media_reference.start_frame + 1
            for image in image_files[1:]:
                # Get current image number
                current_file_number = int(
                    re.search(
                        f"(?<={media_reference.name_prefix})(\d+?)(?={media_reference.name_suffix})",
                        image.name,
                    )[0]
                )

                # Add missing frames
                while frame < current_file_number:
                    missing_image_name = "".join(
                        [
                            media_reference.name_prefix,
                            str(frame).zfill(media_reference.frame_zero_padding),
                            media_reference.name_suffix,
                        ]
                    )
                    sequence.elements.append(missing_image_name)

                    # Increment current frame
                    frame += 1

                # Append the current image
                sequence.elements.append(image.name)

                # Increment current frame
                frame += 1

        # Any other media reference
        else:
            if type(media_reference).__name__ == "UnknownSchema":
                self.report(
                    {"ERROR"},
                    f"Installed version of OpenTimelineIO doesn't handle the schema of {clip}. Please upgrade it.",
                )
                return

            media_path = Path(expandvars(media_reference.target_url))

            # Check for relative path
            if not media_path.is_absolute():
                media_path = source_editing_file_dir.joinpath(media_path).resolve()

            # Is it defined to be a sound or actually a sound file
            if kind == "Audio" or media_path.suffix.lower() in extensions["audio"]:
                sequence = sequences_stack.new_sound(
                    name=clip.name,
                    filepath=str(media_path),
                    channel=channel_index,
                    frame_start=frame_start,
                )

            # Fallback on guessing if image or movie
            else:
                if media_path.suffix.lower() in extensions["image"]:  # Image
                    sequence = sequences_stack.new_image(
                        name=clip.name,
                        filepath=str(media_path),
                        channel=channel_index,
                        frame_start=frame_start,
                    )
                elif media_path.suffix.lower() in extensions["video"]:  # Movie
                    sequence = sequences_stack.new_movie(
                        name=clip.name,
                        filepath=str(media_path),
                        channel=channel_index,
                        frame_start=frame_start,
                    )

        # Apply clip attributes to sequence
        self.set_data(clip, sequence)

        return sequence

    @staticmethod
    def set_data(
        element: Union[Stack, Clip],
        sequence: Union[MetaSequence, MovieSequence, SoundSequence],
    ):
        """Set data from element to sequence.

        :param element: OpenTimelineIO Element
        :param sequence: Blender sequence
        """
        # Offset
        trimmed_range = element.trimmed_range()
        frame_offset_start = otio.opentime.to_frames(trimmed_range.start_time)
        # Test for optimize, don't modify VSE when not needed
        if sequence.frame_offset_start != frame_offset_start:
            # Keep frame start
            sequence_frame_start = sequence.frame_final_start

            # Set offset
            sequence.frame_offset_start = frame_offset_start

            # Put back frame start in timeline
            sequence.frame_start = sequence_frame_start - frame_offset_start

        # Duration
        duration = otio.opentime.to_frames(trimmed_range.duration)
        # Test for optimize, don't modify VSE when not needed
        if sequence.frame_final_duration != duration:
            sequence.frame_final_duration = duration

        # Data
        name = element.name
        if name:
            sequence.name = name

        # Metadata
        for key, value in element.metadata.items():
            sequence[key] = value


# Only needed if you want to add into a dynamic menu
def menu_func_import(self, context):
    self.layout.operator(
        SEQUENCER_OT_import_editing.bl_idname,
        text=f"Editing (.{', .'.join(otio.adapters.suffixes_with_defined_adapters(read=True))})",
    )


# This allows you to right click on a button and link to documentation
# def add_object_manual_map():
#     url_manual_prefix = "https://docs.blender.org/manual/en/latest/"
#     url_manual_mapping = (
#         ("bpy.ops.mesh.add_object", "scene_layout/object/types.html"),
#     )
#     return url_manual_prefix, url_manual_mapping
class VSEIO_Track(bpy.types.PropertyGroup):
    """Store Track data from imported timeline.

    Mostly here to be able to access fulfilled data from imported editing file.
    Not meant to be handled during editing.
    """

    name: bpy.props.StringProperty(name="Track Name")


class VSEIO_addon_preferences(bpy.types.AddonPreferences):
    """
    During registering each adapter module will append it's own properties in the addon preferences.
    That way we can display them in the UI, and their settings will be saved over sessions and are not bound
    to a scene.
    """

    bl_idname = __package__
    bl_description = (
        "Addon preferences of vse_io. Contains options that can customize export / import ",
        "for different formats",
    )
    adapters: bpy.props.PointerProperty(type=adapters.VSEIO_Adapter_Properties)
    path_remapping: bpy.props.EnumProperty(
        name="Path Remapping",
        items=[
            ("ABSOLUTE", "Absolute", "Convert all paths to absolute"),
            (
                "RELATIVE_TO_TARGET",
                "Relative to Target Directory",
                "Convert all paths to be relative to target directory",
            ),
            ("NONE", "None", "Leave paths as is"),
        ],
        default="NONE",
    )


classes = [
    VSEIO_addon_preferences,
    SEQUENCER_OT_export_editing,
    SEQUENCER_OT_import_editing,
    VSEIO_Track,
]


def register():

    # Register adapters
    adapters.register()

    # Register classes
    for cls in classes:
        bpy.utils.register_class(cls)

    # Store tracks names
    bpy.types.Scene.tracks = bpy.props.CollectionProperty(type=VSEIO_Track)

    # Export
    bpy.types.TOPBAR_MT_file_export.append(menu_func_export)

    # Import
    bpy.types.TOPBAR_MT_file_import.append(menu_func_import)
    # bpy.utils.register_manual_map(add_object_manual_map)


def unregister():

    # Unregister adapters
    adapters.unregister()

    for cls in classes:
        bpy.utils.unregister_class(cls)

    del bpy.types.Scene.tracks

    # Export
    bpy.types.TOPBAR_MT_file_export.remove(menu_func_export)

    # Import
    bpy.types.TOPBAR_MT_file_import.remove(menu_func_import)
    # bpy.utils.unregister_manual_map(add_object_manual_map)


if __name__ == "__main__":
    register()
