from typing import Dict, Any, List, Union
import bpy

from .abstracts import VSEIO_Generic_Properties, GenericAdapter


class VSEIO_AAF_Properties(VSEIO_Generic_Properties):

    """
    Propertygroup that represents the options that can be set for this adapter.
    Will be registered under addon_preferences.adapters.aaf.
    """

    # For more info check: https://github.com/PixarAnimationStudios/OpenTimelineIO/wiki/Working-with-OpenTimelineIO-and-AAF
    export_use_empty_mob_ids: bpy.props.BoolProperty(
        name="Use Empty Mob Ids",
        description="Let OTIO create dummy MobIDS. Media will be offline in target software and needs to be relinked.",
        get=lambda self: True,  # We always need that option, make it uneditable but show in UI for transparency
    )


class Adapter(GenericAdapter):

    name: str = "AAFAdapter"

    @classmethod
    def draw_export_options(
        cls, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        """
        Draws the export options defined in the property group of this adapter that
        can modify the export.
        """
        addon_prefs = cls.get_addon_prefs()

        row = layout.row(align=True)
        row.prop(addon_prefs.adapters.aaf, "export_use_empty_mob_ids")

    @classmethod
    def get_export_options(cls) -> Dict[str, Any]:
        """
        Returns all the export options defined in the property group of this adapter as dictionary.
        """
        addon_prefs = cls.get_addon_prefs()
        return addon_prefs.adapters.aaf.get_export_options()

    @classmethod
    def get_write_options(cls) -> Dict[str, Any]:
        """
        Returns the export options that are valid for otio.adapters.write_to_file(). So they can be passed as **kwargs.
        """
        addon_prefs = cls.get_addon_prefs()
        return {"use_empty_mob_ids": addon_prefs.adapters.aaf.export_use_empty_mob_ids}

    @classmethod
    def get_import_options(cls) -> Dict[str, Any]:
        """
        Returns all the import options defined in the property group of this adapter as dictionary.
        """
        addon_prefs = cls.get_addon_prefs()
        return addon_prefs.adapters.aaf.get_import_options()


# ----------------REGISTER--------------

classes = [VSEIO_AAF_Properties]


def register():
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister():
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
