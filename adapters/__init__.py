import bpy
from . import xml_adapter, aaf_adapter


class VSEIO_Adapter_Properties(bpy.types.PropertyGroup):
    xml: bpy.props.PointerProperty(type=xml_adapter.VSEIO_XML_Properties)
    aaf: bpy.props.PointerProperty(type=aaf_adapter.VSEIO_AAF_Properties)


# ----------------REGISTER--------------

classes = [VSEIO_Adapter_Properties]


def register() -> None:

    # Register adapters
    xml_adapter.register()
    aaf_adapter.register()

    # Register main adapter property group
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister() -> None:

    # Unregister adapters
    xml_adapter.unregister()
    aaf_adapter.unregister()

    # Unregister main adapter property group
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
