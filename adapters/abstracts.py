from typing import Dict, Any, List, Union

from opentimelineio.schema import Timeline

import bpy


class VSEIO_Generic_Properties(bpy.types.PropertyGroup):

    """
    Propertygroup that represents the options that can be set for this adapter.
    Will be registered under addon_preferences.adapters.XX.
    """

    def get_export_options(self) -> Dict[str, Any]:
        # Returning self.__dict__ does not work, as it only picks up class properties
        # when they changed their default value. We need to take route via .bl_rna.
        d = {}
        for key in self.bl_rna.properties.keys():
            if key not in ["rna_type", "name"]:
                if key.startswith("export"):
                    d[key] = getattr(self, key)
        return d

    def get_import_options(self) -> Dict[str, Any]:
        # Returning self.__dict__ does not work, as it only picks up class properties
        # when they changed their default value. We need to take route via .bl_rna.
        d = {}
        for key in self.bl_rna.properties.keys():
            if key not in ["rna_type", "name"]:
                if key.startswith("import"):
                    d[key] = getattr(self, key)
        return d


class GenericAdapter:

    name: str = "GenericAdapter"

    # ---------UTILS---------------------

    @classmethod
    def get_addon_prefs(cls) -> bpy.types.AddonPreferences:
        return bpy.context.preferences.addons[__package__].preferences

    # ---------PUBLIC INTERFACE-----------

    @classmethod
    def draw_export_options(
        cls, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        """
        Draws the export options defined in the property group of this adapter that
        can modify the export.
        """
        return None

    @classmethod
    def get_export_options(cls) -> Dict[str, Any]:
        """
        Returns all the export options defined in the property group of this adapter as dictionary.
        """
        return {}

    @classmethod
    def get_write_options(cls) -> Dict[str, Any]:
        """
        Returns the export options that are valid for otio.adapters.write_to_file(). So they can be passed as **kwargs.
        """
        return {}

    @classmethod
    def get_import_options(cls) -> Dict[str, Any]:
        """
        Returns all the import options defined in the property group of this adapter as dictionary.
        """
        return {}

    @classmethod
    def add_export_metadata(
        cls, context: bpy.types.Context, timeline: Timeline
    ) -> Timeline:
        """
        Adds metadata to timeline object and children depending on the options that are set for export.
        """
        return timeline
