from typing import Dict, Any, List, Union

from opentimelineio.schema import Timeline

import bpy

from .abstracts import VSEIO_Generic_Properties, GenericAdapter


class VSEIO_XML_Properties(VSEIO_Generic_Properties):

    """
    Propertygroup that represents the options that can be set for this adapter.
    Will be registered under addon_preferences.adapters.xml.
    """

    export_timeline_metadata: bpy.props.BoolProperty(
        name="Timeline Metadata",
        description="Will append metadata (resolution, framerate) to timeline",
    )


timeline_metadata_base = {
    "fcp_xml": {
        "media": {
            "video": {
                "format": {
                    "samplecharacteristics": {
                        "height": "856",
                        "pixelaspectratio": "square",
                        "rate": {"ntsc": "FALSE", "timebase": "24"},
                        "width": "2048",
                    }
                }
            },
        },
        "rate": {"ntsc": "FALSE", "timebase": "24"},
    }
}


class Adapter(GenericAdapter):

    name: str = "XMLAdapter"

    @classmethod
    def draw_export_options(
        cls, context: bpy.types.Context, layout: bpy.types.UILayout
    ) -> None:
        """
        Draws the export options defined in the property group of this adapter that
        can modify the export.
        """
        addon_prefs = cls.get_addon_prefs()

        row = layout.row(align=True)
        row.prop(addon_prefs.adapters.xml, "export_timeline_metadata")

    @classmethod
    def get_export_options(cls) -> Dict[str, Any]:
        """
        Returns all the export options defined in the property group of this adapter as dictionary.
        """
        addon_prefs = cls.get_addon_prefs()
        return addon_prefs.adapters.xml.get_export_options()

    @classmethod
    def get_write_options(cls) -> Dict[str, Any]:
        """
        Returns the export options that are valid for otio.adapters.write_to_file(). So they can be passed as **kwargs.
        """
        return {}

    @classmethod
    def get_import_options(cls) -> Dict[str, Any]:
        """
        Returns all the import options defined in the property group of this adapter as dictionary.
        """
        addon_prefs = cls.get_addon_prefs()
        return addon_prefs.adapters.xml.get_import_options()

    @classmethod
    def add_export_metadata(
        cls, context: bpy.types.Context, timeline: Timeline
    ) -> Timeline:
        """
        Adds metadata to timeline object and children depending on the options that are set for export.
        """
        addon_prefs = cls.get_addon_prefs()

        # If export timeline info option enabled
        if addon_prefs.adapters.xml.export_timeline_metadata:
            cls._add_metadata_to_timeline(context, timeline)

        return timeline

    @classmethod
    def _add_metadata_to_timeline(
        cls, context: bpy.types.Context, timeline: Timeline
    ) -> Timeline:
        timeline.metadata.update(timeline_metadata_base)
        timeline.metadata["fcp_xml"]["media"]["video"]["format"][
            "samplecharacteristics"
        ]["height"] = context.scene.render.resolution_y
        timeline.metadata["fcp_xml"]["media"]["video"]["format"][
            "samplecharacteristics"
        ]["width"] = context.scene.render.resolution_x
        timeline.metadata["fcp_xml"]["media"]["video"]["format"][
            "samplecharacteristics"
        ]["rate"]["timebase"] = context.scene.render.fps
        timeline.metadata["fcp_xml"]["rate"]["timebase"] = context.scene.render.fps
        return timeline


# ----------------REGISTER--------------

classes = [VSEIO_XML_Properties]


def register() -> None:
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister() -> None:
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
