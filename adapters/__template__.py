from typing import Dict, Any, List, Union
import bpy

from .abstracts import VSEIO_Generic_Properties, GenericAdapter


class VSEIO_XX_Properties(VSEIO_Generic_Properties):

    """
    Propertygroup that represents the options that can be set for this adapter.
    Will be registered under addon_preferences.adapters.XX.
    """

    pass


class Adapter(GenericAdapter):
    pass


# ----------------REGISTER--------------

classes = [VSEIO_XX_Properties]


def register() -> None:
    for cls in classes:
        bpy.utils.register_class(cls)


def unregister() -> None:
    for cls in reversed(classes):
        bpy.utils.unregister_class(cls)
