"""Utils functions for VSE IO.
"""

from typing import List, Tuple
import opentimelineio as otio


def get_audio_extensions() -> frozenset:
    """Get all file extensions that should be considered as audio.

    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".wav",
            ".ogg",
            ".oga",
            ".mp3",
            ".mp2",
            ".ac3",
            ".aac",
            ".flac",
            ".wma",
            ".eac3",
            ".aif",
            ".aiff",
            ".m4a",
            ".mka",
        ]
    )


def get_video_extensions() -> frozenset:
    """Get all file extensions that should be considered as video.

    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".avi",
            ".mpg",
            ".mpeg",
            ".dvd",
            ".vob",
            ".mp4",
            ".mov",
            ".dv",
            ".ogg",
            ".ogv",
            ".mkv",
            ".flv",
            ".webm",
        ]
    )


def get_image_extensions() -> frozenset:
    """Get all file extensions that should be considered as image.

    Extracted from Blender source code at https://developer.blender.org/diffusion/B/browse/master/source/blender/imbuf/intern/util.c%2495
    """
    return frozenset(
        [
            ".bmp",
            ".cin",
            ".dds",
            ".dpx",
            ".exr",
            ".hdr",
            ".j2c",
            ".jp2",
            ".jpeg",
            ".jpg",
            ".pdd",
            ".png",
            ".psb",
            ".psd",
            ".rgb",
            ".rgba",
            ".sgi",
            ".tga",
            ".tif",
            ".tiff",
            ".tx",
        ]
    )


def get_available_adapters_items(read=False, write=False) -> List[Tuple[str, str, str]]:
    """Build available adapters as enum items.

    :param read: Only read formats
    :param write: Only write formats
    :return: List of items as `[(id, name, description), ...]`
    """
    # Get OTIO adaters
    defined_adapters = otio.adapters.suffixes_with_defined_adapters(
        read=read, write=write
    )

    # Build items
    active_plugin_manifest = otio.plugins.ActiveManifest()
    adapters_plugins = getattr(active_plugin_manifest, "adapters")
    items = []
    for plug in adapters_plugins:
        info = plug.plugin_info_map()

        for suffix in plug.suffixes:  # One item by suffix
            if suffix in defined_adapters:
                items.append((f".{suffix}", f"{info['name']} (.{suffix})", info["doc"]))

    return items
